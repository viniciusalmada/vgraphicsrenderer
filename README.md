## Pipeline

Its needed to know an abstract element to be rendered. 
From this element its retrieved its:
-vertices positions
-color
-indices

## Prepare to render in GL3

-Create vertex buffer
  -Pass a vector with one vertex data per position
-Create layout to pass to vertex array
-Create index buffer
-Create shaders
-Create renderer