#pragma once

#include "vgr_element.hpp"

namespace VGR
{
  /**
   * @brief Rectangle shape class that implements IElement interface
   *
   * @par A Rectangle is represented by a bottom-left corner position
   * and width and height values.
   *
   * @par The bottom-left is designed to be vertex 0, bottom-right is
   * vertex 1 and so on:
   *
   * 3          2
   *  ┌────────┐
   *  │        │
   *  │        │
   *  └────────┘
   * 0          1
   * 
   * The triangles to render this rectangle are formed by triangles:
   * T1 = 0-1-2
   * T2 = 0-2-3
   *
   */
  class Rectangle : public IElement
  {
  public:
    Rectangle(Point botLeftPt, int w, int h);

    void SetColor(const Color& c) { this->color = c; }

    std::vector<Point> RetrievePositions() const override;

    Color RetrieveColor() const override;

    std::vector<unsigned int> RetrieveIndices() const override;

  private:
    Color color = Color::WHITE;
    Point bot_left_point;
    int width;
    int height;
  };
} // namespace VGR
