#ifndef VGR_POINT
#define VGR_POINT

namespace VGR
{
  class Point
  {
  public:
    Point(int x, int y) : x(x), y(y) {}

    Point operator+(const Point& pt) const;

    int GetX() const { return x; }
    int GetY() const { return y; }

  private:
    int x;
    int y;
  };
}

#endif // !VGR_POINT
