#ifndef VGR_GL_HANDLER
#define VGR_GL_HANDLER

#include "GL3/gl3_index_buffer.h"
#include "GL3/gl3_layout.h"
#include "GL3/gl3_renderer.h"
#include "GL3/gl3_vertex_buffer.h"
#include "vgr_element.hpp"

#include <memory>
#include <set>
#include <string>

namespace VGR
{
  class VertexInfo : public GL3::Layout
  {
  public:
    VertexInfo(VGR::Point pt, VGR::Color color) : point(pt), color(color) {}
    VertexInfo() : point({ 0, 0 }), color({ 0, 0, 0 }) {}

    struct Data
    {
      int x;
      int y;
      float red;
      float green;
      float blue;
    };

    Data data() const
    {
      return { point.GetX(), point.GetY(), color.Red(), color.Green(),
               color.Blue() };
    };

    std::vector<GL3::VertexBufferElement> getLayout() const override;

  private:
    VGR::Point point;
    VGR::Color color;
  };

  class GLHandler
  {
  public:
    GLHandler(unsigned int width, unsigned int height);

    void UpdateBuffers(std::set<std::shared_ptr<IElement>> elements);

  private:
    GL3::Shader CreateShader() const;

    GL3::RendererData InitRenderer() const;

    static const std::string VERTEX_SHADER_SRC;
    static const std::string FRAGMENT_SHADER_SRC;

    unsigned int width = 0;
    unsigned int height = 0;
    GL3::Renderer renderer;
  };
} // namespace VGR

#endif // !VGR_GL_HANDLER