#ifndef VGR_ELEMENT
#define VGR_ELEMENT

#include "vgr_color.hpp"
#include "vgr_point.hpp"

#include <array>
#include <vector>

namespace VGR
{
  class IElement
  {
  public:
    virtual std::vector<Point> RetrievePositions() const = 0;

    virtual Color RetrieveColor() const = 0;

    virtual std::vector<unsigned int> RetrieveIndices() const = 0;
  };
}

#endif // !VGR_ELEMENT
