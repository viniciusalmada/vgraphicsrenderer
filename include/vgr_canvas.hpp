#ifndef VGR_CANVAS
#define VGR_CANVAS

#include "vgr_element.hpp"
#include "vgr_gl_handler.hpp"

#include <set>
#include <memory>


namespace VGR
{
  class Canvas
  {
  public:
    Canvas(int w, int h);

    void DrawElement(std::shared_ptr<IElement> element);

    void Render() const;
  
  private:
    int width;
    int height;

    GLHandler gl_handler;

    std::set<std::shared_ptr<IElement>> elements;
  };
}

#endif // !VGR_CANVAS
