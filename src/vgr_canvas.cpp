#include "vgr_canvas.hpp"

VGR::Canvas::Canvas(int w, int h) : width(w), height(h), gl_handler(w, h)
{
}

void VGR::Canvas::DrawElement(std::shared_ptr<IElement> element)
{
  elements.insert(element);

  gl_handler.UpdateBuffers(elements);
}
