#include "vgr_point.hpp"

VGR::Point VGR::Point::operator+(const VGR::Point& pt) const
{
  return { this->x + pt.x, this->y + pt.y };
}