#include "vgr_rectangle.hpp"

VGR::Rectangle::Rectangle(VGR::Point botLeftPt, int w, int h)
    : bot_left_point(botLeftPt), width(w), height(h)
{
}

std::vector<VGR::Point> VGR::Rectangle::RetrievePositions() const
{
  VGR::Point bot_right = bot_left_point + VGR::Point{ width, 0 };
  VGR::Point top_right = bot_left_point + VGR::Point{ width, height };
  VGR::Point top_left = bot_left_point + VGR::Point{ 0, height };

  return { bot_left_point, bot_right, top_right, top_left };
}

VGR::Color VGR::Rectangle::RetrieveColor() const { return this->color; }

std::vector<unsigned int> VGR::Rectangle::RetrieveIndices() const
{
  return { 0, 1, 2, 0, 2, 3 };
}