#include "vgr_gl_handler.hpp"

#include "GL3/gl3_shader.h"

#include <algorithm>

std::vector<GL3::VertexBufferElement> VGR::VertexInfo::getLayout() const
{
  return { GL3::VertexBufferElement(GL_INT, 2),
           GL3::VertexBufferElement(GL_FLOAT, 3) };
}

const std::string VGR::GLHandler::VERTEX_SHADER_SRC = R"glsl(
  #version 330 core
  
  layout(location = 0) in ivec2 position;
  layout(location = 1) in vec3 color;

  out vec4 frag_color;

  uniform mat2 scale_mat;

  void main()
  {
    scale_position = scale_mat * position;
    gl_Position = vec4(scale_position, 1.0, 1.0);
    fragColor = vec4(color, 1.0);
  }

)glsl";

const std::string VGR::GLHandler::FRAGMENT_SHADER_SRC = R"glsl(
  #version 330 core

  in vec4 frag_color;

  out vec4 color;

  void main()
  {
    color = frag_color;
  }
)glsl";

VGR::GLHandler::GLHandler(unsigned int width, unsigned int height)
    : width(width), height(height), renderer({ InitRenderer() })
{
}

GL3::Shader VGR::GLHandler::CreateShader() const
{
  float* scale_matrix = new float[4];
  scale_matrix[0] = 1.0f / static_cast<float>(width);
  scale_matrix[1] = 0.0f;
  scale_matrix[2] = 1.0f / static_cast<float>(height);
  scale_matrix[3] = 0.0f;

  // Start by creating and compile shaders
  GL3::ShaderSource vertex_src{ VERTEX_SHADER_SRC, GL3::ShaderType::VERTEX };
  GL3::ShaderSource frag_src{ FRAGMENT_SHADER_SRC, GL3::ShaderType::FRAGMENT };

  GL3::Shader shader{ vertex_src, frag_src };

  shader.bind();
  shader.setUniformMatrix2fv("scale_mat", scale_matrix);
  shader.unbind();

  delete[] scale_matrix;

  return shader;
}

GL3::RendererData VGR::GLHandler::InitRenderer() const
{
  GL3::Shader shader = CreateShader();

  GL3::IndexBuffer index_buffer{ nullptr, 0 };
  GL3::VertexBuffer vertex_buffer{ nullptr, 0 };

  GL3::VertexArray vertex_array{ vertex_buffer, VertexInfo{} };

  return GL3::RendererData{ vertex_array, index_buffer, shader };
}

void VGR::GLHandler::UpdateBuffers(std::set<std::shared_ptr<IElement>> elements)
{
  // Update first vertex buffer
  {
    std::vector<VertexInfo::Data> vertices_data;

    for (const std::shared_ptr<IElement> element : elements)
    {
      std::vector<VGR::Point> positions = element->RetrievePositions();
      VGR::Color color = element->RetrieveColor();
      for (const VGR::Point pt : positions)
      {
        VertexInfo info{ pt, color };
        vertices_data.push_back(info.data());
      }
    }

    unsigned int size_of_vertices =
      static_cast<unsigned int>(vertices_data.size()) *
      static_cast<unsigned int>(sizeof(VertexInfo::Data));
    this->renderer.updateVertexBuffer(vertices_data.data(), size_of_vertices);
  }

  // Update second index buffer
  {
    unsigned int current_max_index = 0;
    std::vector<unsigned int> indices_data;

    for (const std::shared_ptr<IElement> element : elements)
    {
      const std::vector<unsigned int> indices = element->RetrieveIndices();
      for (const auto i : indices)
      {
        indices_data.push_back(i + current_max_index);
      }

      const unsigned int max_index =
        *std::max_element(indices.cbegin(), indices.cend());
      current_max_index += max_index + 1;
    }

    this->renderer.updateIndexBuffer(indices_data.data(), static_cast<unsigned int>(indices_data.size()));
  }
}