#ifndef GL3_HEADER
#define GL3_HEADER

#include "gl3_defs.h"

#include <GL/glew.h>

#define GlCall(x)                                                              \
  GL3::Log::GLClearError();                                                    \
  x;                                                                           \
  GL3::Log::GLLogCall(#x, __FILE__, __LINE__)

namespace GL3
{
  class GL3_DECLSPEC Log
  {
  private:
    static bool isLogStarted;

    static void GLStartLogger();

  public:
    static void PrintMessage(const char* message);

    static void GLStopLogger();

    static void GLClearError();

    static bool GLLogCall(const char* function, const char* file, int line);
  };
}

#endif // !GL3_HEADER
