#ifndef GL3_VERTEX_BUFFER
#define GL3_VERTEX_BUFFER

#include "gl3_defs.h"

namespace GL3
{
  class GL3_DECLSPEC VertexBuffer
  {
  private:
    unsigned int bufferId;
    unsigned int size;

  public:
    VertexBuffer(const void* data, unsigned int size);
    void freeBuffer();

    void setData(const void* data, unsigned int newSize);

    void bind() const;
    void unbind() const;
  };
}

#endif // GL3_VERTEX_BUFFER