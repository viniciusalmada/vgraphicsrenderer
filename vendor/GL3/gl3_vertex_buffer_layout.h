#ifndef GL3_VERTEX_BUFFER_LAYOUT
#define GL3_VERTEX_BUFFER_LAYOUT

#include "gl3_defs.h"

#include <GL/glew.h>
#include <vector>

namespace GL3
{
  class GL3_DECLSPEC VertexBufferElement
  {
  public:
    VertexBufferElement(unsigned int type, int count);

    unsigned int type;
    int count;

  public:
    unsigned long long bytesCount() const;
  };
}

#endif // GL3_VERTEX_BUFFER_LAYOUT
