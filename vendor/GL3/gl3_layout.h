#ifndef GL3_LAYOUT_HEADER
#define GL3_LAYOUT_HEADER

#include "gl3_vertex_buffer_layout.h"
#include "gl3_defs.h"

#include <vector>
namespace GL3
{
  class GL3_DEFINES Layout
  {
  public:
    virtual std::vector<VertexBufferElement> getLayout() const = 0;

    int stride() const
    {
      unsigned long strideSize = 0;
      const auto elements = getLayout();
      for (const VertexBufferElement& element : elements)
      {
        switch (element.type)
        {
        case GL_INT:
          strideSize += sizeof(int) * element.count;
          break;
        case GL_DOUBLE:
          strideSize += sizeof(double) * element.count;
          break;
        case GL_FLOAT:
          strideSize += sizeof(float) * element.count;
          break;
        }
      }
      return static_cast<int>(strideSize);
    }
  };
}

#endif // GL3_LAYOUT_HEADER
